section .rodata

first_offset equ 8
second_offset equ 16

section .text

%define SPACE_SYM   0x20
%define TAB_SYM     0x9
%define NEWLINE_SYM 0xA
%define NULL_SYM    0x0

; System calls
%define SYS_READ  0
%define SYS_WRITE 1
%define SYS_EXIT  60

; File descriptors
%define STDIN     0
%define STDOUT    1


global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; string_length( rdi ) - > ( rax ):
;   ==длина строки включая 0-указатель==
;
; rdi - указатель строки
; rax - длинна строки
;
string_length:
    xor rax, rax

    .loop:
        cmp byte[rdi+rax], NULL_SYM
        je .end
        inc rax
        jmp .loop

    .end:
        ret



; print_string( rdi ):
;   usage - rax, rsi, rdx
;   ==вывод строки==
;
; rdi - указатель строки
;
print_string:
    push rdi
    call string_length   ; rax - содержит длинну
    pop rdi

    mov rdx, rax

    mov rax, SYS_WRITE   ; 'write' syscall number
    mov rsi, rdi         ; string address
    mov rdi, STDOUT      ; stdout descriptor
    syscall
    ret


; print_char( rdi ):
;   usage - rax, rsi, rdx
;   ==выводит символ==
;
; rdi - символ для вывода
;
print_char:
    enter 0, 0

    push rdi
    mov     rax, SYS_WRITE   ; 'write' syscall number
    mov     rsi, rsp         ; string address
    mov     rdi, STDOUT      ; stdout descriptor
    mov     rdx, 1           ; string length in bytes
    syscall
    leave
    ret



; print_newline():
;   ==перевод строки==
;
print_newline:
    mov rdi, NEWLINE_SYM
    jmp print_char



; print_uint( rdi ):
;   usage - r8
;   ==вывод беззнакового 32-битного числа в десятичном формате==
;
; rdi - содержит число в 16 формате
;
print_uint:
    enter 32, 0
    mov rax, rdi
    mov rdi, rbp
    mov r8, 10

    dec rdi
    mov byte[rdi], 0

    .loop:
        xor rdx, rdx    ; в eax число, которое делим на 10
        div r8         ; edx хранит остаток от деления
        add dl, '0'     ; в al храним целочисленное деление
        dec rdi
        mov byte[rdi], dl
        test rax, rax     ; сохраянем процент от деления        ; al == 0 => выводим числа
        jnz .loop

    .print:
        call print_string
        leave
        ret


; print_int( rdi ):
;   usage - r8
;   ==вывод знакового 32-битного числа в десятичном формате==
;
; rdi - содержит число в 16 формате
;
print_int:
    enter 0, 0

    cmp rdi, 0
    jge .out        ; если число >= 0, выводим как беззнаковое

    neg rdi         ; иначе делаем отрицание
    push rdi        ; выводим минус и число как беззнаковое
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    pop rdi

    .out:
        call print_uint
        leave
        ret



; string_equals( rdi, rsi ) -> ( rax ):
;   usage - rcx, r9
;   == проверка равенства строк (str1==str2 : 1 ? 0) ==
;
; rdi - указатель на 1-ую строку
; rsi - указатель на 2-ую строку
; rax - равны ли строки
;
string_equals:
    enter 16, 0

    cmp rdi, rsi        ; если равны указатели
    je .yes

    push rsi
    push rdi
    call string_length   ; получили длинну 1 строки и сохранили
    pop rdi
    pop rsi
    mov [rbp-first_offset], rax

    xchg rsi, rdi

    push rsi
    push rdi
    call string_length   ; получили длинну 2 строки и сохранили
    pop rdi
    pop rsi
    mov [rbp-second_offset], rax
.r:
    cmp [rbp-first_offset], rax    ; если не равны длины строк
    jne .not

    mov rcx, 0           ; текущий индекс

    .loop:
        mov r9b, byte [rdi+rcx]
        mov r10b, byte [rsi+rcx]
        cmp r9b, r10b
        jne .not

        cmp rcx, [rbp-first_offset]
        je .yes
        inc rcx
        jmp .loop

    .not:
        mov rax, 0
        leave
        ret

    .yes:
        mov rax, 1
        leave
        ret


; read_char() -> ( rax ):
;   usage - rdi, rsi, rdx
;   ==возвращает один символ из stdin, 0 если достигнут конец потока==
;
; rax - введенный символ
;
read_char:
    enter 8, 0

    xor rax, rax    ; 'read' syscall number
    xor rdi, rdi    ; stdout descriptor
    mov rsi, rsp    ; address to write
    mov rdx, 1      ; string length in bytes
    syscall

    cmp rax, 0
	je .end
    pop rax

    .end:
        leave
        ret



; read_word( rdi, rsi ) - > ( rax, rdx ):
;   usage - r12, r13, r15
;   == читает в буфер слово из stdin, пропуская пробельные символы в начале ==
;   == если слово больше размера буфера, вернуть 0 ==
;   == дописывает к слову нуль-терминатор ==
;
; rdi - адрес начала буфера
; rsi - размер буфера
; rax - адрес буфера
; rdx - длина слова
;
read_word:
    enter 0, 0

    push r12
    push r13
    push r14
    push r15

    mov r12, rdi
    mov r13, rsi
    xor r15, r15

    inc r13

    test r13, r13
    je .bad             ; если не перешло, значит буффер >= 1

    .garbage:
        call read_char
        test rax, rax
        jz .bad

        cmp rax, SPACE_SYM
        je .garbage

        cmp rax, TAB_SYM
        je .garbage

        cmp rax, NEWLINE_SYM
        je .garbage

    .read:
        mov [r12+r15], rax
        inc r15

        test rax, rax
        je .good
        cmp rax, SPACE_SYM
        je .good
        cmp rax, TAB_SYM
        je .good
        cmp rax, NEWLINE_SYM
        je .good

        cmp r15, r13
        je .bad

        call read_char
        jmp .read

    .bad:
        xor rax, rax
        xor rdx, rdx
        jmp .end

    .good:
        dec r15
        mov rax, r12
        mov rdx, r15

    .end:
        pop r15
        pop r14
        pop r13
        pop r12
        leave
        ret

; parse_uint( rdi ) - > ( rax, rdx ):
;   usege - r8, r9
;   == пытается прочитать из начала строки беззнаковое число ==
;   == если число прочитать не удалось, вернуть 0 в rdx ==
;
; rdi - адрес буфера
; rax - число
; rdx - длина слова
;
parse_uint:
    enter 0, 0

    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r9, 10

    .loop:
        movzx r8, byte[rdi+rdx]

        sub r8, '0'
        jb .end
        cmp r8, 9
        ja .end

        push rdx
        mul r9
        pop rdx

        add rax, r8
        inc rdx
        jmp .loop

    .end:
        leave
        ret



; parse_int( rdi ) - > ( rax, rdx ):
;   usege - r8, r9
;   == пытается прочитать из начала строки знаковое число ==
;   == пробелы между знаком и числом не разрешены ==
;   == если число прочитать не удалось, вернуть 0 в rdx ==
;
; rdi - адрес буфера
; rax - число
; rdx - длина слова
;
parse_int:
    enter 0, 0
    mov r9, 10

    cmp byte[rdi], '-'
    je .sign

    call parse_uint
    leave
    ret

    .sign:
        inc rdi
        call parse_uint
        test rdx, rdx
        jz .sign_nf
        inc rdx

    .sign_nf:
        neg rax
        leave
        ret



; string_copy( rdi, rsi, rdx ) -> ( rax ):
;   usege - rcx, r8
;   == копирует строку в буфер ==
;   == возвращает длину строки если она умещается в буфер, иначе 0 ==
;
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длина буфера
; rax - длина строки
;
string_copy:
    enter 0, 0

    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi

    xor rcx, rcx

    cmp rdx, rax
    jb .out

    .copy:
        mov r8b, byte[rdi+rcx]
        mov byte[rsi+rcx], r8b
        cmp rcx, rax
        je .end
        inc rcx
        jmp .copy

    .out:
        xor rax, rax

    .end:
        leave
        ret
